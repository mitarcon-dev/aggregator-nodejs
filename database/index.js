'use strict'

const chalk = require('chalk')
const Mongoose = require('mongoose')
const { database: dbConfig } = require('../config')

let db = null
let Aggregator = null
let createdConnection = false

module.exports = function () {
  if (!createdConnection) {
    createdConnection = true
    Mongoose.connect(dbConfig.connectionString, { useNewUrlParser: true })
    const db = Mongoose.connection

    db.on('error', console.error.bind(console, 'connection error:'))
    console.log(`${chalk.green('[aggregator-db]')} Connected to MongoDB database`)

    Aggregator = require('./lib/aggregator')(db)
  }

  return {
    Aggregator
  }
}
