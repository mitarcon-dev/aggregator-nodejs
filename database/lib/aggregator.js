'use strict'

let AggregatorSchema = null

function fetch ({ building_id, unit_id, aggregatorType, startDate, endDate }) {
  return AggregatorSchema.findOne({
    building_id,
    unit_id,
    aggregatorType,
    startDate,
    endDate
  }).exec()
}

function store (object) {
  return new AggregatorSchema(object).save()
}

function update (object) {
  return AggregatorSchema.findOneAndUpdate({ _id: object._id }, object, { new: true })
}

async function deleteAll () {
  await AggregatorSchema.deleteMany({})
}

module.exports = function (db) {
  AggregatorSchema = require('../schema/aggregator')(db)

  return {
    deleteAll,
    fetch,
    store,
    update
  }
}
