'use strict'

const aggregator = {
  building_id: String,
  unit_id: String,
  startDate: Date,
  endDate: Date,
  aggregatorType: String,
  total: Number
}

module.exports = function (db) {
  return db.model('Aggregator', aggregator)
}
