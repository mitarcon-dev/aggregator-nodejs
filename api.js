'use strict'

const aggregator = require('./aggregator')
const debug = require('debug')('aggregator:api:routes')
const express = require('express')

const api = express.Router()

api.post('/aggregator', (req, res) => {
  debug(`Objeto de entrada es ${JSON.stringify(req.body)}`)
  aggregator.addAggregator(req.body)
  res.send('OK')
})

api.post('/aggregator/debt', (req, res) => {
  debug(`Objeto de entrada es ${JSON.stringify(req.body)}`)
  aggregator.addAggregatorDebt(req.body)
  res.send('OK')
})

api.delete('/aggregator', (req, res) => {
  debug('Eliminar todo el contenido de la coleccion')
  aggregator.deleteAll()
  res.send('OK Eliminar')
})

module.exports = api
