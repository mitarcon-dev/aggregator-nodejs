'use strict'

const { Aggregator } = require('../database')()
const debug = require('debug')('aggregator:api:aggregator')
const moment = require('moment')

const { aggregatorType } = require('./constants')

const { TRANSACTION, DAY, WEEK, FORTNIGHT, MONTH, TRIMESTER, SEMESTER, ANNUAL } = aggregatorType

async function createOrUpdate ({ building_id, unit_id, aggregatorT, intervalDays, total }) {
  debug(`createOrUpdate
    buildingId: ${building_id}
    unitId: ${unit_id}
    aggregatorT: ${aggregatorT}
    intervalDays: start: ${intervalDays[aggregatorT].start} - end: ${intervalDays[aggregatorT].end}
    --------------------------
  `)

  let agg = null
  try {
    if (aggregatorT !== TRANSACTION) {
      agg = await Aggregator.fetch({
        building_id,
        unit_id,
        aggregatorType: aggregatorT,
        startDate: intervalDays[aggregatorT].start,
        endDate: intervalDays[aggregatorT].end
      })
    }
  } catch (error) {
    console.log(err)
    return
  }

  console.log('Respuesta fue ', agg)

  if (!agg) {
    let salida = await Aggregator.store({
      building_id,
      unit_id,
      aggregatorType: aggregatorT,
      startDate: intervalDays[aggregatorT].start,
      endDate: intervalDays[aggregatorT].end,
      total
    })
    console.log('salida crear ', salida)
  } else {
    agg.total = updateTotalAggregator(agg.total, total)
    console.log('suma es ', agg.total)
    let salida = await Aggregator.update(agg)
  }
}

function updateTotalAggregator (oldValue, newValue) {
  if (!oldValue) {
    oldValue = 0
  }
  if (!newValue) {
    return oldValue
  }
  return oldValue + newValue
}

// function fetch () {

// }

// function show () {

// }

function addAggregator (operation) {
  const {
    created_at,
    building_id,
    unit_id,
    total
  } = operation

  console.log(operation)
  if (!created_at) {
    console.log('Objeto con estrucutra incorrecta')
    return
  }

  let intervalDays = getIntervalsDays(created_at)
  let param

  // For building
  debug('######## BUILDING ########')
  // Transaction

  // Other aggregator
  param = {
    building_id,
    unit_id,
    operation,
    intervalDays,
    total
  }
  createOrUpdate(Object.assign({ aggregatorT: TRANSACTION }, param))
  createOrUpdate(Object.assign({ aggregatorT: DAY }, param))
  createOrUpdate(Object.assign({ aggregatorT: WEEK }, param))
  createOrUpdate(Object.assign({ aggregatorT: FORTNIGHT }, param))
  createOrUpdate(Object.assign({ aggregatorT: MONTH }, param))
  createOrUpdate(Object.assign({ aggregatorT: TRIMESTER }, param))
  createOrUpdate(Object.assign({ aggregatorT: SEMESTER }, param))
  createOrUpdate(Object.assign({ aggregatorT: ANNUAL }, param))

  // // For unit
  // debug('######## UNIT ########')
  // // Transaction

  // // Other aggregator
  // param = {
  //   building_id,
  //   unit_id,
  //   operation,
  //   intervalDays
  // }
  // createOrUpdate(Object.assign({ aggregatorT: TRANSACTION }, param))
  // createOrUpdate(Object.assign({ aggregatorT: DAY }, param))
  // createOrUpdate(Object.assign({ aggregatorT: WEEK }, param))
  // createOrUpdate(Object.assign({ aggregatorT: FORTNIGHT }, param))
  // createOrUpdate(Object.assign({ aggregatorT: MONTH }, param))
  // createOrUpdate(Object.assign({ aggregatorT: TRIMESTER }, param))
  // createOrUpdate(Object.assign({ aggregatorT: SEMESTER }, param))
  // createOrUpdate(Object.assign({ aggregatorT: ANNUAL }, param))

  // console.log(intervalDays)
  return intervalDays
}

function addAggregatorDebt (debt) {

}

function getIntervalsDays (date) {
  let creationAt = moment(date).startOf('day')
  debug(`getIntervalsDays: date ${creationAt}`)

  return {
    [TRANSACTION]: {
      start: creationAt,
      end: creationAt
    },
    [DAY]: {
      start: creationAt,
      end: creationAt
    },
    [WEEK]: {
      start: moment(creationAt).startOf('week'),
      end: moment(creationAt).endOf('week')
    },
    [FORTNIGHT]: getIntervalsDaysFortnight(moment(creationAt)),
    [MONTH]: {
      start: moment(creationAt).startOf('month'),
      end: moment(creationAt).endOf('month')
    },
    [TRIMESTER]: getIntervalsDaysTrimester(moment(creationAt)),
    [SEMESTER]: getIntervalsDaysSemester(moment(creationAt)),
    [ANNUAL]: {
      start: moment(creationAt).startOf('year'),
      end: moment(creationAt).endOf('year')
    }
  }
}

/* +
* date: momenet class
*/
function getIntervalsDaysFortnight (date) {
  const dayInNumber = date.date()
  let start, end

  if (dayInNumber < 16) {
    start = moment(date).startOf('month')
    end = date.date(15)
  } else {
    start = date.date(16)
    end = moment(date).endOf('month')
  }

  return {
    start,
    end
  }
}

/* +
* date: momenet class
*/
function getIntervalsDaysTrimester (date) {
  const monthInNumber = date.month()

  if ([0, 1, 2].includes(monthInNumber)) {
    return {
      start: moment(date).month(0).startOf('month'),
      end: moment(date).month(2).endOf('month')
    }
  } else if ([3, 4, 5].includes(monthInNumber)) {
    return {
      start: moment(date).month(3).startOf('month'),
      end: moment(date).month(5).endOf('month')
    }
  } else if ([6, 7, 8].includes(monthInNumber)) {
    return {
      start: moment(date).month(6).startOf('month'),
      end: moment(date).month(8).endOf('month')
    }
  } else if ([9, 10, 11].includes(monthInNumber)) {
    return {
      start: moment(date).month(9).startOf('month'),
      end: moment(date).month(11).endOf('month')
    }
  }
}

/* +
* date: momenet class
*/
function getIntervalsDaysSemester (date) {
  const monthInNumber = date.month()

  if ([0, 1, 2, 3, 4, 5].includes(monthInNumber)) {
    return {
      start: moment(date).month(0).startOf('month'),
      end: moment(date).month(5).endOf('month')
    }
  } else if ([6, 7, 8, 9, 10, 11].includes(monthInNumber)) {
    return {
      start: moment(date).month(6).startOf('month'),
      end: moment(date).month(11).endOf('month')
    }
  }
}

function deleteAll () {
  Aggregator.deleteAll()
}

module.exports = {
  addAggregator,
  addAggregatorDebt,
  deleteAll
}
