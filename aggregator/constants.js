'use strict'

module.exports = {
  operatorType: {
    PAYMENT: 'payment',
    DEBT: 'debt',
    CONDONATION: 'condonation'
  },
  aggregatorType: {
    TRANSACTION: 'transaction',
    DAY: 'day',
    WEEK: 'week',
    FORTNIGHT: 'fortnight',
    MONTH: 'month',
    TRIMESTER: 'trimester',
    SEMESTER: 'semester',
    ANNUAL: 'annual'
  }
}
