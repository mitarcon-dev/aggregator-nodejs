'use strict'

const api = require('./api')
const bodyParser = require('body-parser')
const chalk = require('chalk')
const { server: configServer } = require('./config')
const express = require('express')

const app = express()
const port = process.env.PORT || configServer.port

// Express error handle
function handleFatalError (err) {
  console.error(`${chalk.red('[fatal error]')} ${err.message}`)
  console.error(err.stack)
  process.exit(1)
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api', api)

if (!module.parent) {
  process.on('uncaughtException', handleFatalError)
  process.on('unhandledRejection', handleFatalError)

  app.listen(port, () => {
    console.log(`${chalk.green('[aggregator-api]')} server listening on port ${port}`)
  })
}

module.exports = app
